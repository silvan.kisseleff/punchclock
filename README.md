# M223: Punchclock
Dies ist das Abgabe Projekt von Silvan Kisseleff welches im Rahmen des Modul 223 entwickelt wurde.

Die Applikation kann Zeiterfassungen anzeigen, bearbeiten, hinzufügen und löschen. Es ist ein Multiusersystem, sprich
man kann sich mit einem Account anmelden. Die Applikation verfügt auch noch über eine Userverwaltung welche analog zu
den Zeiterfassungen funktioniert.
Um die Funktionen zu benützen, muss man angemeldet sein.
## Setup
Folgende Schritte befolgen um loszulegen:
1. Sicherstellen, dass JDK 12 installiert und in der Projekteinstellung `SDK` definiert ist.
1. Ins Verzeichnis der Applikation wechseln und über die Kommandozeile mit `./gradlew bootRun` oder `./gradlew.bat bootRun` starten oder mit dem PHP-Storm `Run PunchClock [dev]` ausführen.
1. Unittest mit `./gradlew test` oder `./gradlew.bat test` ausführen.
1. Ein ausführbares JAR kann mit `./gradlew bootJar` oder `./gradlew.bat bootJar` erstellt werden.

Die Datenbank Konsole ist unter folgendem Link verfügbar: http://localhost:8081/h2-console, die Parameter sind in der
Konfiguration ersichtlich `src/main/resources/application.properties`.
 
Folgende Routen stehen in dem Frontend zu Verfügung
- http://localhost:8081/login/index.html
- http://localhost:8081/signup/index.html
- http://localhost:8081/login/index.html
- http://localhost:8081/interface/index.html
- http://localhost:8081/interface/user.html

Standartmässig wird ein User schon in der Datenbank abgespeichert.

**Username:** admin  
**Passwort:** password

Wenn man weitere Entries hinzufügt oder bearbeitet, hat man folgende Projekte standardmässig zur Verfügung:

| ID | Name       |
|----|------------|
| 1  | Projekt 1  |
| 2  | Projekt 2  |

Wenn man weitere User hinzufügt oder bearbeitet, hat man folgende Teams standardmässig zur Verfügung:

| ID | Name       |
|----|------------|
| 1  | Team 1  |
| 2  | Team 2  |