package ch.zli.m223.punchclock.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.List;

@Entity
public class User {

    // Primary Key
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // Relation to User
    @JsonBackReference("user")
    @OneToMany(mappedBy = "user")
    private List<Entry> entries;

    // Relation to Team
    @ManyToOne
    @JoinColumn(name = "team_id")
    private Team team;

    // Field: username: The unique username used to login
    @Column(nullable = false, unique = true)
    private String username;

    // Field: password: The hashed password for the User
    @Column(nullable = false)
    private String password;

    // getter & setter
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Entry> getEntries() {
        return entries;
    }

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
