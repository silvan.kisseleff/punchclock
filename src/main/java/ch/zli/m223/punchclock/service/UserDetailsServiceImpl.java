package ch.zli.m223.punchclock.service;

import ch.zli.m223.punchclock.repository.UserRepository;
import ch.zli.m223.punchclock.domain.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Collections.emptyList;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private UserRepository userRepository;

    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    // create a User in the Repository
    public User createUser(User user) {
        return userRepository.saveAndFlush(user);
    }

    // return a list of all Users from the Repository
    public List<User> findAll() {
        return userRepository.findAll();
    }

    // find a User by id and change its values and save it again (update a User)
    public User updateUser (Long userId, User user) {
        User userToUpdate = userRepository.getOne(userId);
        userToUpdate.setUsername(user.getUsername());
        userToUpdate.setPassword(user.getPassword());
        userToUpdate.setTeam(user.getTeam());
        return userRepository.save(userToUpdate);
    }

    // delete a User by Id from the Repository
    public void deleteUser(Long userId) { userRepository.deleteById(userId); }

    // Find a User by Username
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), emptyList());
    }
}
