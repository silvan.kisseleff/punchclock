package ch.zli.m223.punchclock.service;

import ch.zli.m223.punchclock.domain.Entry;
import ch.zli.m223.punchclock.domain.User;
import ch.zli.m223.punchclock.repository.EntryRepository;
import ch.zli.m223.punchclock.repository.UserRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EntryService {
    private EntryRepository entryRepository;
    private UserRepository userRepository;

    public EntryService(EntryRepository entryRepository, UserRepository userRepository) {
        this.entryRepository = entryRepository;
        this.userRepository = userRepository;
    }

    // create a new Entry in the Repository
    public Entry createEntry(Entry entry) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User currentUser = userRepository.findWhereUsernameIs(principal.toString());
        entry.setUser(currentUser);
        return entryRepository.saveAndFlush(entry);
    }

    // return the Entries from the Repository
    public List<Entry> findAll() {
        // get the entry where id is 1 (just to show that JPQL is used)
        Entry test = entryRepository.findWhereUserIdIs1();
        return entryRepository.findAll();
    }

    // find the Entry to update and change it's values and Save it again (update an Entry)
    public Entry updateEntry (Long entryId, Entry entry) {
        Entry entryToUpdate = entryRepository.getOne(entryId);
        entryToUpdate.setCheckIn(entry.getCheckIn());
        entryToUpdate.setCheckOut(entry.getCheckOut());
        entryToUpdate.setProject(entry.getProject());
        return entryRepository.save(entryToUpdate);
    }

    // delete an Entry from the Repository
    public void deleteEntry(Long entryId) { entryRepository.deleteById(entryId); }
}
