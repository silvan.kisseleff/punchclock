package ch.zli.m223.punchclock.repository;

import ch.zli.m223.punchclock.domain.Entry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface EntryRepository extends JpaRepository<Entry, Long> {
    // Query to demonstrate JPQL
    @Query("select e from Entry e where e.id = 1")
    Entry findWhereUserIdIs1();
}
