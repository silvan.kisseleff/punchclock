package ch.zli.m223.punchclock.security;

public class SecurityConstants {
    public static final String SECRET = "TheUltraTopSecretKeyNoOneWillEverFindOut1";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/users/sign-up";
    public static final String LOGIN_URL = "/login/index.html";
    public static final String SIGN_UP_INTERFACE_URL = "/signup";
    public static final String H2_CONSOLE_ULR = "/h2-console/*";
}
