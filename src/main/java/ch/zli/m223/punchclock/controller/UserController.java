package ch.zli.m223.punchclock.controller;


import ch.zli.m223.punchclock.domain.User;
import ch.zli.m223.punchclock.service.UserDetailsServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ch.zli.m223.punchclock.repository.UserRepository;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

// Listen to all Request that have the Path /api/users or /api/users/{id} whereby id a path parameter is
@RestController
@RequestMapping(value = {"/api/users", "/api/users/{id}"})
public class UserController {

    private UserRepository userRepository;
    private UserDetailsServiceImpl userService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    // We need the user Repository to save a entry to a user (owner of entry)
    // We need the bCryptPasswordEncoder to hash the Password when creating / updating a User
    public UserController(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder, UserDetailsServiceImpl userService) {
        this.userRepository = userRepository;
        this.userService = userService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    // Route to create a new User
    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public void signUp(@RequestBody User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    // Route to get a list of all Users
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<User> getAllUsers() {
        return userService.findAll();
    }

    // Route to update an existing User
    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public User updateUser(@Valid @PathVariable Long id, @Valid @RequestBody User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return userService.updateUser(id, user);
    }

    // Route to delete a User
    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    public void deleteUser(@Valid @PathVariable Long id) { userService.deleteUser(id); }
}
