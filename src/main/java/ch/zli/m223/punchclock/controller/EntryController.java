package ch.zli.m223.punchclock.controller;

import ch.zli.m223.punchclock.domain.Entry;
import ch.zli.m223.punchclock.service.EntryService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

// Listen to all Request that have the Path /api/entries or /api/entries/{id} whereby id a path parameter is
@RestController
@RequestMapping(value = {"/api/entries", "/api/entries/{id}"})
public class EntryController {
    private EntryService entryService;

    public EntryController(EntryService entryService) {
        this.entryService = entryService;
    }

    // Route to get all Entries
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Entry> getAllEntries() {
        return entryService.findAll();
    }

    // Route to create a new Entry
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Entry createEntry(@Valid @RequestBody Entry entry) {
        return entryService.createEntry(entry);
    }

    // Route to update an existing Entry
    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public Entry updateEntry(@Valid @PathVariable Long id, @Valid @RequestBody Entry entry) { return entryService.updateEntry(id, entry); }

    // Route to delete a Entry
    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    public void deleteEntry(@Valid @PathVariable Long id) { entryService.deleteEntry(id); }
}
