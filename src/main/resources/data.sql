-- Create Projects
INSERT INTO PROJECT VALUES (1, 'Project 1'), (2, 'Project 2');

-- Create Teams
INSERT INTO TEAM VALUES (1, 'Team 1'), (2, 'Team 2');

-- Create a default User with the username: admin and password: password
    INSERT INTO USER VALUES (1, '$2a$10$1UBb18Z4V4SL30qONMjHLeThzT13FJ3U1oj8DLhAZmpWQ5I3VEglq', 'admin', 1);